# file_chooser.py: file chooser dialogs for opening and outputting files
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

from __future__ import annotations
from os.path import basename
from gi.repository import Gtk
from upscaler.filters import get_format_filters, supported_filters, image_filters
from gettext import gettext as _
from typing import Any, Callable, Optional, TYPE_CHECKING

if TYPE_CHECKING:
    # Only import UpscalerWindow when we are checking the typings
    from upscaler.window import UpscalerWindow

class FileChooser:

    @staticmethod
    def open_file(parent: UpscalerWindow, *args: Any) -> None:
        """Open an image file from a file chooser dialog."""
        def load_file(_dialog: Gtk.FileChooserNative, response: Gtk.ResponseType) -> None:

            # Return if user cancels
            if response != Gtk.ResponseType.ACCEPT:
                return

            # Run if the user selects an image
            file = dialog.get_file()
            if file is None:
                return
            parent.on_load_file(file)

        dialog = Gtk.FileChooserNative.new(
            title=_('Select an image'),
            parent=parent,
            action=Gtk.FileChooserAction.OPEN,
            accept_label=None,
            cancel_label=None
        )
        dialog.set_modal(True)
        dialog.connect('response', load_file)
        dialog.add_filter(supported_filters())
        dialog.show()

    @staticmethod
    def output_file(parent: UpscalerWindow,
                    default_name: str,
                    good: Callable[[str], None],
                    bad: Callable[[Optional[str]], None],
                    *args: Any) -> None:
        """Select output location from file chooser dialog."""
        def upscale_content(_dialog: Gtk.FileChooserNative, response: Gtk.ResponseType) -> None:

            # Set output file path if user selects a location
            if response != Gtk.ResponseType.ACCEPT:
                bad(None)
                return

            # Get all filters
            filters = []
            for filter in get_format_filters('image'):
                filters.append(filter.split('/').pop())

            # Check if output file has a file extension or format is supported
            file = dialog.get_file()
            if file is None:
                bad(None)
                return

            output_file_path = file.get_path()
            if output_file_path is None:
                bad(None)
                return

            if '.' not in basename(output_file_path):
                bad(_('No file extension was specified'))
                return

            elif basename(output_file_path).split('.').pop().lower() not in filters:
                filename = basename(output_file_path).split('.').pop()
                bad(_(f'’{filename}’ is an unsupported format'))
                return

            print(f'Output file: {output_file_path}')
            good(output_file_path)

        dialog = Gtk.FileChooserNative.new(
            title=_('Select output location'),
            parent=parent,
            action=Gtk.FileChooserAction.SAVE,
            accept_label=None,
            cancel_label=None
        )
        dialog.set_modal(True)
        dialog.connect('response', upscale_content)
        dialog.add_filter(image_filters())
        dialog.set_current_name(default_name)
        dialog.show()
